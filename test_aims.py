from nose.tools import assert_equal

import aims 

def test_ints():
    numbers = [3, 4, 5]
    obs = aims.std(numbers)
    exp = 0.81649658092772603
    assert_equal(obs, exp)

def test_negative():
    numbers = [-1, -2, -3, -4, -5]
    obs = aims.std(numbers)
    exp = 1.4142135623730951
    assert_equal(obs, exp)

def test_floats():
    numbers = [3.4, 4.5, 5.6]
    obs = aims.std(numbers)
    exp = 0.89814623902049862
    assert_equal(obs, exp)

def test_zeros():
    numbers = [0, 0, 0, 0]
    obs = aims.std(numbers)
    exp = 0
    assert_equal(obs, exp)


def test_avgthom1():
    files = ['data/thomas/0213']
    obs = aims.avg_range(files)
    exp = 8
    assert_equal(obs, exp)

def test_avgthom2():
    files = ['data/thomas/0241']
    obs = aims.avg_range(files)
    exp = 9
    assert_equal(obs, exp)

def test_avgthom3():
    files = ['data/thomas/0287']
    obs = aims.avg_range(files)
    exp = 7
    assert_equal(obs, exp)

def test_avggerd1():
    files = ['data/gerdal/Data0541']
    obs = aims.avg_range(files)
    exp = 8
    assert_equal(obs, exp)

def test_avggerd2():
    files = ['data/gerdal/Data0543']
    obs = aims.avg_range(files)
    exp = 0
    assert_equal(obs, exp)


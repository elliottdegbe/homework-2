#!/usr/bin/env python

print "Welcome to the AIMS module"
import numpy as np

def std(x):
    b=range(len(x))
    if x==[]:
        return "Cannot find the Standard Deviation of an empty set. Please enter a number."
    x_bar = np.mean(x)
    ssquare = (sum((x-x_bar)**2))/float(len(x))
    return np.sqrt(ssquare)


def avg_range(y):
    files = []
    ranges = []
    for location in y:
        files.append(open(location))

    for entry in files:
        for line in entry:
            if 'Range' in line:
                ranges.append(float(line[7]))
    return sum(ranges)/len(ranges)




if __name__ == "__main__":
    main()

